class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index
    @all_ratings = Movie.ratings
    
    session[:ratings] ||= Hash.new
    session[:sort_by] ||= String.new

    params[:ratings] ||= Hash.new
    params[:sort_by] ||= String.new

    @select_ratings ||= Hash.new
    @select_ratings = params[:ratings].collect {|key,value| key } if params[:ratings]
    

    if params[:ratings].empty? && session[:ratings].empty?
      @select_ratings = @all_ratings
      if params[:sort_by] == 'title'
        @movies = Movie.where(:rating => @select_ratings).order(:title)
        @title_header = 'hilite'
      elsif params[:sort_by] == 'release_date'
        @movies = Movie.where(:rating => @select_ratings).order(:release_date)
        @release_date_header = 'hilite'
      else
        @movies = Movie.where(:rating => @select_ratings)  
      end
    elsif !params[:ratings].empty?
      if params[:sort_by] == 'title'
        @movies = Movie.where(:rating => @select_ratings).order(:title)
        @title_header = 'hilite'
      elsif params[:sort_by] == 'release_date'
        @movies = Movie.where(:rating => @select_ratings).order(:release_date)
        @release_date_header = 'hilite'
      else
        @movies = Movie.where(:rating => @select_ratings)  
      end
    else
      @select_ratings = session[:ratings].collect {|key,value| key } if session[:ratings]
      if session[:sort_by] == 'title'
        @movies = Movie.where(:rating => @select_ratings).order(:title)
        @title_header = 'hilite'
      elsif session[:sort_by] == 'release_date'
        @movies = Movie.where(:rating => @select_ratings).order(:release_date)
        @release_date_header = 'hilite'
     else
        @movies = Movie.where(:rating => @select_ratings)  
      end
      flash.keep
      redirect_to :action => "index", :ratings => session[:ratings], :sort_by => session[:sort_by]
    end

    session[:sort_by] = params[:sort_by]
    session[:ratings] = params[:ratings]
  end



  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
