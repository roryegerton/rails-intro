class Movie < ActiveRecord::Base
  attr_accessible :title, :rating, :description, :release_date

  def self.ratings
    return self.pluck(:rating).uniq
  end
end
